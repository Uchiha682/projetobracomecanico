﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;
using uPLibrary.Networking.M2Mqtt.Utility;
using uPLibrary.Networking.M2Mqtt.Exceptions;
using Vuforia;

using System;

public class vbInteraction : MonoBehaviour, IVirtualButtonEventHandler
{
	VirtualButtonBehaviour[] virtualButtonBehaviours;
	string vbName;
	public GameObject btn1FirstPanel, btn2FirstPanel, btn1SecondPanel, btn2SecondPanel, motoLigada, motoDesligada;

	private MqttClient client;

	void Start()
	{
		client = new MqttClient("m16.cloudmqtt.com", 11620 , false , null ); 

		client.MqttMsgPublishReceived += client_MqttMsgPublishReceived; 

		string clientId = Guid.NewGuid().ToString(); 
		client.Connect(clientId, "avfsaghw", "2R2YRMR0Qdbv"); 

		client.Subscribe(new string[] { "esp/test" }, new byte[] { MqttMsgBase.QOS_LEVEL_AT_MOST_ONCE }); 

		virtualButtonBehaviours = GetComponentsInChildren<VirtualButtonBehaviour>();

		for (int i = 0; i < virtualButtonBehaviours.Length; i++)
			virtualButtonBehaviours[i].RegisterEventHandler(this);
		
	}

	string msg = "";

	void client_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e) 
		{ 
			try {
				msg = System.Text.Encoding.UTF8.GetString (e.Message).Trim ();

			} catch {
			 
			}
			Debug.Log ("Recebido do MQTT: " + msg);
	} 
		
	public void OnButtonPressed(VirtualButtonBehaviour vb)
	{
		vbName = vb.VirtualButtonName;
		if (vbName == "VirtualButton1") { 
			VirtualButton1 ();
			Deactivate ();
			Btn1 ();
			msgLigado ();
		} 

		if (vbName == "VirtualButton2") {
				VirtualButton2 ();
				Deactivate ();
				Btn2 ();
				msgDesligado ();
		}
	}

	public void OnButtonReleased(VirtualButtonBehaviour vb)
	{

	}

	void VirtualButton1()
	{
		if (btn1FirstPanel.activeInHierarchy) {
			btn1FirstPanel.SetActive (false);
			btn2FirstPanel.SetActive (true);
		}
	}

	void VirtualButton2()
	{
		if (btn2SecondPanel.activeInHierarchy) {
			btn1FirstPanel.SetActive (true);
			btn2FirstPanel.SetActive (false);
		}
	}

	void Deactivate()
	{
		motoLigada.SetActive(false);
	}

	void Btn1()
	{
		motoLigada.SetActive(true);
		motoDesligada.SetActive(false);
	}

	void Btn2()
	{
		motoLigada.SetActive(false);
		motoDesligada.SetActive(true);
	}

	void Btn3()
	{
		motoLigada.SetActive(false);
		motoDesligada.SetActive(true);
	}


	void msgLigado()
	{
		client.Publish("esp/test", System.Text.Encoding.UTF8.GetBytes("1"), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
		Debug.Log("enviado para o servidor!");
	}

	void msgDesligado ()
	{
		client.Publish("esp/test", System.Text.Encoding.UTF8.GetBytes("5"), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
		Debug.Log("enviado para o servidor!");
	}
		

	void Update () {
		if (Input.GetMouseButtonDown (0)) {
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			RaycastHit hit;

			if (Physics.Raycast (ray, out hit)) {
				if (hit.collider.tag == "btn1FirstPanel") {
					VirtualButton1 ();
					Deactivate ();
					Btn1 ();
					msgLigado ();
				}

				if (hit.collider.tag == "btn2FirstPanel") {
					VirtualButton2 ();
					Deactivate ();
					Btn2 ();
					msgDesligado ();
				}
			}
		}
			 
	}

}
